package avenuecode.com.ac.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "image")
public class Image implements Serializable {

	private static final long serialVersionUID = 2170772762398804910L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("codigo")
	private Long codigo;

	@JsonProperty("img")
	private String img;

	
	@JoinColumn(name="cod_produto",referencedColumnName="codProduto")
	@JsonProperty("codProduto")
	private Long codProduto;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public Long getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(Long codProduto) {
		this.codProduto = codProduto;
	}

	
	
}
