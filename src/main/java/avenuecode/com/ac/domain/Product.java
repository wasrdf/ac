package avenuecode.com.ac.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "Product")
public class Product implements Serializable {

	private static final long serialVersionUID = 5242868741501181508L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonProperty("codProduto")
	private Long codProduto;

	@JsonProperty("name")
	private String name;
	
	@JsonProperty("description")
	private String description;
	
	//@OneToMany(mappedBy="codProduto", fetch = FetchType.LAZY)
	//@JsonProperty("images")
	@Transient
	private List<Image> images = new ArrayList<>();
	
	@JoinColumn(name="cod_produto_parent")
	@JsonProperty("productParent")
	private Long productParent;
	
	@Transient
	private List<Product> products;
	
	
	public Product() {
		super();
		this.products = new ArrayList<>();
	}

	public Long getCodProduto() {
		return codProduto;
	}

	public void setCodProduto(Long codProduto) {
		this.codProduto = codProduto;
	}

	public List<Image> getImages() {
		return images;
	}

	public void setImages(List<Image> images) {
		this.images = images;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getProductParent() {
		return productParent;
	}

	public void setProductParent(Long productParent) {
		this.productParent = productParent;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
