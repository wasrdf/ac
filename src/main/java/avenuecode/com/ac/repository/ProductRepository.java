package avenuecode.com.ac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import avenuecode.com.ac.domain.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
	
	@Query(value = "SELECT p FROM Product p WHERE p.codProduto = ?1 ")
	public Product findById(Long id);
	
	
	List<Product> findByProductParent(Long id);
	
}		
