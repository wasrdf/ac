package avenuecode.com.ac.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import avenuecode.com.ac.domain.Image;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {

	List<Image> findOneByCodProduto(Long codProduto);
}
