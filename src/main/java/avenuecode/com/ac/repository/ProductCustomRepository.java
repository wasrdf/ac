package avenuecode.com.ac.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import avenuecode.com.ac.domain.Product;

@Repository
public class ProductCustomRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	@Transactional(propagation = Propagation.REQUIRED)
	public List<Product> getAll() {
		
		
		
		TypedQuery<Product> query = em.createQuery("SELECT p FROM Product p  ",Product.class);
		
		return query.getResultList();
	}
	
	
}
