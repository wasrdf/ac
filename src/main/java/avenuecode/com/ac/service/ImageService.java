package avenuecode.com.ac.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import avenuecode.com.ac.domain.Image;
import avenuecode.com.ac.repository.ImageRepository;

@Service
public class ImageService {
	
	@Autowired
	private ImageRepository repository;
	
	public void salvarImage(Image img) {
		repository.save(img);
	}
	
	public void deletar(Long id) {
		repository.delete(id);
	}
	
	public List<Image> getAll() {
		return repository.findAll();
	}
	
	public Image findOne(Long id) {
		return repository.findOne(id);
	}
	
}
