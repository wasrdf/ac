package avenuecode.com.ac.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import avenuecode.com.ac.domain.Image;
import avenuecode.com.ac.domain.Product;
import avenuecode.com.ac.repository.ImageRepository;
import avenuecode.com.ac.repository.ProductCustomRepository;
import avenuecode.com.ac.repository.ProductRepository;

@Service
@Transactional
public class ProductService {

	@Autowired
	private ProductRepository repository;

	@Autowired
	private ImageRepository imageRepository;

	@Autowired
	ProductCustomRepository customRepository;

	public Product save(Product p) throws Exception {
		Product prod = repository.save(p);
		return prod;
	}

	/**
	 * quando um produto pai é deletado os filhos e as imagens também são.
	 * 
	 * @param codigo
	 */
	public void delete(Long codigo) {

		List<Product> products = findProductsByParent(codigo);
		if (!products.isEmpty()) {
			for (Product p : products) {
				repository.delete(p); // deleta os filhos
			}
		}

		List<Image> images = imageRepository.findOneByCodProduto(codigo);
		if (!images.isEmpty()) {
			for (Image image : images) {
				imageRepository.delete(image); // deleta as imagens
			}
		}

		repository.delete(codigo);
	}

	public Product findById(Long codigo) {
		return repository.findById(codigo);
	}

	public Product findByIdWithRelations(Long codigo) {
		Product product = repository.findById(codigo);
		product.setProducts(repository.findByProductParent(product.getCodProduto()));
		product.setImages(imageRepository.findOneByCodProduto(product.getCodProduto()));
		return repository.findById(codigo);
	}

	public List<Image> findWithImage(Long codigo) {
		return imageRepository.findOneByCodProduto(codigo);
	}

	public List<Product> findProductsByParent(Long codigo) {
		return repository.findByProductParent(codigo);
	}

	public List<Product> getAllProducts() {
		return repository.findAll();
	}

	public List<Product> getAllProdIncludingRelationShip() {
		List<Product> products = repository.findAll();
		for (Product product : products) {
			product.setProducts(repository.findByProductParent(product.getCodProduto()));
			product.setImages(imageRepository.findOneByCodProduto(product.getCodProduto()));
		}
		return products;
	}
}
