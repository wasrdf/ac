package avenuecode.com.ac.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import avenuecode.com.ac.domain.Image;
import avenuecode.com.ac.domain.Product;
import avenuecode.com.ac.service.ProductService;

@RestController
@RequestMapping("/api/product")
public class ProductController {

	@Autowired
	private ProductService service;

	@GetMapping("/getAll")
	public List<Product> getAll() {
		return service.getAllProducts();
	}
	
	@GetMapping("/getAllIncludingRelations")
	public List<Product> getAllIncludingRelations() {
		return service.getAllProdIncludingRelationShip();
	}


	@PostMapping("/create")
	public void create(@Valid @RequestBody Product product) {
		try {
			service.save(product);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Product> delete(@PathVariable(value = "id") Long id) {
		Product product = service.findById(id);
		if (product == null) {
			return ResponseEntity.notFound().build();
		}
		service.delete(product.getCodProduto());
		return ResponseEntity.ok().build();
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Product> getById(@PathVariable(value = "id") Long id) {
		Product product = service.findById(id);
		if (product == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(product);
	}
	
	@GetMapping("/findWithRelations/{id}")
	public ResponseEntity<Product> getByIdWithRelations(@PathVariable(value = "id") Long id) {
		Product product = service.findByIdWithRelations(id);
		if (product == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(product);
	}
	
	
	
	@GetMapping("/findProductsByParent/{id}")
	public ResponseEntity<List<Product>> getByParent(@PathVariable(value = "id") Long id) {
		List<Product> products = service.findProductsByParent(id);
		if (products.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(products);
	}
	
	@GetMapping("/findProductImage/{id}")
	public ResponseEntity<List<Image>> getProductAndImage(@PathVariable(value = "id") Long id) {
		List<Image> images = service.findWithImage(id);
		if (images.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(images);
	}
	
	@PutMapping("/update")
	public ResponseEntity<Product> atualizarPessoa(@Valid @RequestBody Product product) {
		if (product.getCodProduto() != null) {
			try {
				service.save(product);
			} catch (Exception e) {
				ResponseEntity.badRequest();	
				e.printStackTrace();
			}
			return ResponseEntity.ok(product);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
}
