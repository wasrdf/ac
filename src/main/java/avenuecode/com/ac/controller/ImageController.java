package avenuecode.com.ac.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import avenuecode.com.ac.domain.Image;
import avenuecode.com.ac.domain.Product;
import avenuecode.com.ac.service.ImageService;

@RestController
@RequestMapping("/api/image")
public class ImageController {
	
	@Autowired
	private ImageService service;
	
	@GetMapping("/getAll")
	public List<Image> getAll() {
		return service.getAll();
	}

	@PostMapping("/create")
	public void create(@Valid @RequestBody Image img) {
		try {
			service.salvarImage(img);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Product> delete(@PathVariable(value = "id") Long id) {
		Image image = service.findOne(id);
		if (image == null) {
			return ResponseEntity.notFound().build();
		}
		service.deletar(image.getCodigo());
		return ResponseEntity.ok().build();
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<Image> getById(@PathVariable(value = "id") Long id) {
		Image image = service.findOne(id);
		if (image == null) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok().body(image);
	}
	
	@PutMapping("/update")
	public ResponseEntity<Image> atualizarImg(@Valid @RequestBody Image image) {
		if (image.getCodigo() != null && image.getCodProduto() != null) {
			try {
				service.salvarImage(image);
			} catch (Exception e) {
				ResponseEntity.badRequest();	
				e.printStackTrace();
			}
			return ResponseEntity.ok(image);
		} else {
			return ResponseEntity.notFound().build();
		}
	}
	

}
