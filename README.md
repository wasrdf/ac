url para criar um produto. 
post. 

body json 
{
   "name":"CESTA BÁSICA",
   "description": "DESCRICAO"
}

http://localhost:9090/api/product/create


Para inserir um filho para o produto pai é necessário passar o código do pai.

http://localhost:9090/api/product/create
post.
body json 
{
   "name":"1 KG ARROZ",
   "description": "DESCRICAO",
   "productParent": 1
}


Para atualizar o produto pai passa o json abaixo.
put 

http://localhost:9090/api/product/update

body json 

{
    "codProduto": 1,
    "name": "CESTA BASICA CARREFOUR",
    "description": "NOVA DESCRICAO"   
}

Para atualizar o pai de um produto filho passa o codigo do pai no JSON

http://localhost:9090/api/product/update

body json 

{
    "codProduto": 2,
    "name": "FEIJAO",
    "description": "nova DESCRICAO"
	"productParent": 1
}

Quando um produto pai é deletado todos os filhos também são deletados.
Passa o codigo do produto pelo parametro.

delete

http://localhost:9090/api/product/delete/1

IMAGEM

Para criar uma imagem é passado no json o código do produto que essa imagem pertence
e também é passado o código base64 da imagem para ser salva no banco como blob.
(obs: normalmente em produção seria passado a pasta onde a imagem seria salva 
mais como é só pra estudo eu estou enviando o base64 da img).

http://localhost:9090/api/image/create

post 

body json 

{
	"img":"base64codigo",
	"codProduto":1
}

ATUALIZAR IMAGEM 

Para atualizar a imagem utilizar a url abaixo.
{
    "codigo": 1,
    "img": "base64codigoNOVO",
    "codProduto": 3
}

DELETE IMAGEM 
Para deletar a imagem utilizar a url abaixo passando como parametro o id da imagem. 

delete

http://localhost:9090/api/image/delete/1


OBTER TODOS OS PRODUTOS EXCLUINDO OS RELACIONAMENTOS
GET 
http://localhost:9090/api/product/getAll

OBTER TODOS OS PRODUTOS INCLUINDO OS RELACIONAMENTOS.
GET
http://localhost:9090/api/product/getAllIncludingRelations

OBTER PRODUTO PELO ID EXCLUINDO OS RELACIONAMENTOS PASSA O ID DO PRODUTO POR PARAMETRO
http://localhost:9090/api/product/find/1

OBTER PRODUTO PELO ID INCLUINDO OS RELACIONAMENTOS PASSA O ID DO PRODUTO POR PARAMETRO 

http://localhost:9090/api/product/findWithRelations/1

OBTER FILHOS PELO ID DO PAI PASSA O CÓDIGO DO PAI POR PARAMETRO
GET 
http://localhost:9090/api/product/findProductsByParent/1


OBTER IMAGENS DE UM DETERMINADO PRODUTO PELO ID DO PRODUTO 
http://localhost:9090/api/product/findProductImage/1

OBS:Eu não criei testes automatizados conforme foi pedido no teste porque eu nunca trabalhei com essas ferramentas de testes como junit. 

O banco utiliza foi o H2.

Para executar o projeto entrar na pasta target onde se encontra o jar.  executar o comando java -jar "nomedojar"
